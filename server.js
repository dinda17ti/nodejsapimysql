var express = require('express'),
    app = express(),
    port = process.env.PORT || 8000,
    bodyParser = require('body-parser'),
    controller = require('./controller');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.get('/palindrome/:id', function(req,res) {
    var value = req.params.id;
    // Masukkan Palindrome disini
    var palindrome = (str) => {
        var len = str.length;
        var mid = Math.floor(len/2);
        for ( var i = 0; i < mid; i++ ) {
            if (str[i] !== str[len - 1 - i]) {
                return false;
            }
        }
        return true;
    }
    var result = palindrome(value);
    return res.status(200).send({
        status: 'OK',
        value: result
    });
})

var routes = require('./routes');
routes(app);

app.listen(port);
console.log('Learn Node JS With Kiddy, RESTful API server started on: ' + port);